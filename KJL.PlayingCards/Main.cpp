// Playing Cards
// Keane Laux

//This is a test comment:)

#include <iostream>
#include <conio.h>

enum Rank // ace high 
{
	TWO = 2, THREE, FOUR, FIVE, SIX, SEVEN, EIGHT, NINE, TEN, JACK, QUEEN, KING, ACE
};

enum Suit
{
	SPADE, DIAMOND, CLUB, HEART
};

struct Card
{
	Rank rank;
	Suit suit;
};

int main()
{
	// testing

	Card card1;
	card1.rank = ACE;
	card1.suit = SPADE;

	Card card2;
	card2.rank = KING;
	card2.suit = SPADE;

	_getch();
	return 0;
}